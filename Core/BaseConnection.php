<?php
/**
 * Created by PhpStorm.
 * User: ghedi
 * Date: 9/20/2017
 * Time: 4:29 PM
 */

namespace Gpws\Core;

require_once './bootstrap.php';

use Gpws\Interfaces\Connection;
use Gpws\Interfaces\Message;

class BaseConnection implements Connection {

    public function __construct(int $connectionId, \resource $resource) {
        $this->connectionId = $connectionId;
        $this->resource = $resource;
    }

    public function getConnectionId(): int {
        return $this->connectionId;
    }

    public function getRemoteAddress(): string {
        if (isNull($this->remoteAddress)){
            $this->remoteAddress = socket_getpeername($this->resource);
        }

        return $this->remoteAddress;
    }

    public function getResource(): \resource {
        return $this->resource;
    }

    public function sendMessage(Message $message) {


    }

    /** @var int $connectionId */
    protected $connectionId;

    /** @var string $remoteAddress */
    protected $remoteAddress = null;

    /** @var \resource $resource */
    protected $resource;
}

