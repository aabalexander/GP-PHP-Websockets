<?php


namespace Gpws\Core;


use Gpws\Interfaces\ListeningSocket;
use Gpws\Interfaces\Message;

class BaseSocket implements ListeningSocket {

    public function __construct(string $ipAddr, int $port) {
        $this->ipAddr = $ipAddr;
        $this->port = $port;
    }

    public function sendMessage(Message $message) {

    }

    public function getMessages(): array {
        // TODO: Implement getMessages() method.
    }

    /**
     * @return ListeningSocket
     * @throws \Exception
     */
    public function listen() {
        $this->socketResource = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
        if ($this->socketResource === false) {
            throw new \Exception('Could not create the socket resource');
        }
    }

    protected $port;

    protected $ipAddr;

    protected $socketResource;
}