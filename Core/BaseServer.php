<?php
/**
 * Created by PhpStorm.
 * User: ghedi
 * Date: 9/22/2017
 * Time: 6:36 AM
 */

namespace Gpws\Core;

use Gpws\Interfaces\EventHandler;
use Gpws\Interfaces\WebsocketServer;

class BaseServer implements WebsocketServer {

    public function run() {
        $this->startup();

        while ($this->isRunning) {

        }

        $this->cleanup();
    }

    protected function startup() {

    }

    protected function cleanup() {

    }

    public function getListenIpAddress(): string {
        return $this->listenAddress;
    }

    public function getListenPort(): int {
        return $this->listenPort;
    }

    public function registerEventHandler(EventHandler $evHandler) {
        $this->eventHandlers[] = $evHandler;
    }

    public function setListenAddress(string $ip, int $port = 80) {
        $this->setListenIpAddress($ip);
        $this->setListenPort($port);
    }

    public function setListenIpAddress(string $ip) {
        $addressParts = explode('.', $ip);
        if (count($addressParts) != 4
         || !filter_var($addressParts[0], FILTER_VALIDATE_INT) || $addressParts[0] > 255 || $addressParts[0] < 0
         || !filter_var($addressParts[1], FILTER_VALIDATE_INT) || $addressParts[1] > 255 || $addressParts[1] < 0
         || !filter_var($addressParts[2], FILTER_VALIDATE_INT) || $addressParts[2] > 255 || $addressParts[2] < 0
         || !filter_var($addressParts[3], FILTER_VALIDATE_INT) || $addressParts[3] > 255 || $addressParts[3] < 0) {
            throw new \InvalidArgumentException('Listen address does not appear to be in the common IPv4 notation format (i.e., 0.0.0.0).');
        }

        $this->listenAddress = $ip;
    }

    public function setListenPort(int $port) {
        $this->listenPort = $port;
    }

    public function shutdown() {
        $this->isRunning = false;
    }

    protected $eventHandlers = array();

    protected $isRunning = true;

    protected $listenAddress = '0.0.0.0'; // Default IP means listen to all traffic.

    protected $listenPort = 80;

}