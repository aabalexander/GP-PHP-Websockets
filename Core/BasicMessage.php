<?php

namespace Gpws\Core;

use Gpws\Interfaces\Connection;
use Gpws\Interfaces\Message;

class BasicMessage implements Message {

    public function __construct(Connection $connection, string $contents) {
        $this->connection = $connection;
        $this->contents = $contents;
    }

    public function getConnection(): Connection {
        return $this->connection;
    }

    public function getContents(): string {
        return $this->contents;
    }

    /** @var Connection $connection */
    protected $connection;

    /** @var string $contents */
    protected $contents;
}