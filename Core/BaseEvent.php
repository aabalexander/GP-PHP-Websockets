<?php


namespace Gpws\Core;


use Gpws\Interfaces\Connection;
use Gpws\Interfaces\Event;
use Gpws\Interfaces\Message;

class BaseEvent implements Event {

    public function __construct(string $type, Connection $connection, Message $message) {
        $this->type = $type;
        $this->connection = $connection;
        $this->message = $message;
    }

    public function getType(): string {
        // TODO: Implement getType() method.
    }

    public function getMessage(): Message {
        // TODO: Implement getMessage() method.
    }

    public function setType(string $type) {
        // TODO: Implement setType() method.
    }

    public function setMessage(Message $message) {
        // TODO: Implement setMessage() method.
    }

    public function getConnection(): Connection {
        // TODO: Implement getConnection() method.
    }

    protected $type;

    protected $connection;

    protected $message;
}