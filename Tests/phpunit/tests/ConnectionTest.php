<?php
/**
 *
 *
 * The question is not what can I add, but what can I take away and still have a useful product.
 *
 * User: ghedipunk
 * Date: 9/16/2017
 * Time: 9:18 PM
 */

declare(strict_types=1);

namespace Tests\Unit;

use Gpws\Interfaces\Connection;
use PHPUnit\Framework\TestCase;

require_once '../../Interfaces/Connection.php';

final class ConnectionTest extends TestCase {

    public function testImplementsConnectionInterface() {
        $foo = null;
        $this->assertInstanceOf(Connection::class, $foo);
    }

}