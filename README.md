This library is in an early Alpha state, and is not useful right now for any purpose other than showing error messages.

It extends concepts I learned in my original WebSockets library, and implements it in a way that's not quite so... 
amateur and embarrassing.

Examples will appear at [my Examples repository](https://gitlab.com/ghedipunk/gp-websocket-examples)

Intended use is:

    <?php
    
    // Assuming your code is in your project's "/code" or similar directory
    require_once (__DIR__ . '/../vendor/autoload.php');
    
    $webSocketsDiContainer = new GpwsDIContainer();
    
    $server = $webSocketsDiContainer->getWebsocketServer();
    
    $server->setListenAddress('0.0.0.0', 8080);
    
    $eventHandler = new MyAppsEventHandler();
    
    $server->registerEventHandler($eventHandler);
    
    $server->run();
    
    // cleanup.
    
I do have Patreon goals.  As my personal financial situation becomes less dire, I'll be more able to provide support for the community.