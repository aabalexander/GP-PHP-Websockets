<?php
/**
 * Created by PhpStorm.
 * User: ghedi
 * Date: 9/21/2017
 * Time: 10:16 AM
 */

namespace Gpws\Interfaces;

interface Event {
    public function getType(): string;
    public function getMessage(): Message;
    public function getConnection(): Connection;

}
