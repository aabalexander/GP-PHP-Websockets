<?php


namespace Gpws\Interfaces;


interface WebsocketServer {
    public function setListenAddress(string $ip, int $port=80);
    public function setListenIpAddress(string $ip);
    public function setListenPort(int $port);
    public function getListenIpAddress(): string;
    public function getListenPort(): int;
    public function run();
    public function shutdown();
    public function registerEventHandler(EventHandler $evHandler);
    public function registerHandshakeHandler(HandshakeHandler $hsHandler);
}