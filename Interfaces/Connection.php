<?php
/**
 * Created by PhpStorm.
 * User: ghedi
 * Date: 9/4/2017
 * Time: 4:11 PM
 */

namespace Gpws\Interfaces;


interface Connection {
    public function getRemoteAddress(): string;
    public function getConnectionId(): int;
    public function getResource(): \resource;
    public function sendMessage(Message $message);
}