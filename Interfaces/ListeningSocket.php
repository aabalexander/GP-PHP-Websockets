<?php


namespace Gpws\Interfaces;


interface ListeningSocket {
    public function listen();
    public function getMessages(): array;
    //public function sendMessage(Message $message);
}