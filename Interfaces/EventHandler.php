<?php
/**
 * Created by PhpStorm.
 * User: ghedi
 * Date: 9/20/2017
 * Time: 5:10 PM
 */

namespace Gpws\Interfaces;


interface EventHandler {
	public function HandleEvent(Event $event);
}
